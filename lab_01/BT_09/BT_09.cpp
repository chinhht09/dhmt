﻿// BT_09.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <math.h>
#include "Dependencies\glew\glew.h"
#include "Dependencies\freeglut\freeglut.h"

const int screenWidth = 640; // khởi tạo chiều dài chiều cho màn hình
const int screenHeight = 480; // khởi tạo chiều rộng cho màn hình
const double R = 150;
const double pi = 3.141592654;

struct GLdoublePoint {
	GLdouble x;
	GLdouble y;
};

void init(void)
{
	glClearColor(1.0, 1.0, 1.0, 1.0); //hàm glClearColor xóa bộ đệm màu
	glShadeModel(GL_FLAT);
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT); // Hàm glClear xóa bộ đệm thành các giá trị đặt trước.
	glPointSize(5.0);
	glColor3f(0.0, 0.0, 0.0);

	glBegin(GL_POINTS);
		GLdoublePoint V1, V2, V3, V4, V5, V0;
		V0.x = screenWidth / 2;
		V0.y = screenHeight / 2;
		V1.x = V0.x;
		V1.y = V0.y + R;
		V2.x = V0.x + R*sin(2 * pi / 5);
		V2.y = V0.y + R*cos(2 * pi / 5);
		V3.x = V0.x + R*sin(pi / 5);
		V3.y = V0.y - R*cos(pi / 5);
		V4.x = V0.x - R*sin(pi / 5);
		V4.y = V0.y - R*cos(pi / 5);
		V5.x = V0.x - R*sin(2 * pi / 5);
		V5.y = V0.y + R*cos(2 * pi / 5);

		glVertex2d(V1.x, V1.y);
		glVertex2d(V2.x, V2.y);
		glVertex2d(V3.x, V3.y);
		glVertex2d(V4.x, V4.y);
		glVertex2d(V5.x, V5.y);
	glEnd();

	glFlush();
}

void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h); //Hàm glViewport thiết lập chế độ xem.
	glMatrixMode(GL_PROJECTION); //Hàm glMatrixMode chỉ định ma trận nào là ma trận hiện tại(Áp dụng các hoạt động ma trận tiếp theo cho ngăn xếp ma trận chiếu.)
	glLoadIdentity(); //Hàm glLoadIdentity thay thế ma trận hiện tại bằng ma trận danh tính.
	gluOrtho2D(0.0, (GLdouble)w, 0.0, (GLdouble)h);//hàm xác định ma trận chiếu hình chính tả 2 chiều.
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv); //glutInit được sử dụng để khởi tạo thư viện GLUT.
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA); //đặt chế độ hiển thị ban đầu.
	glutInitWindowSize(screenWidth, screenHeight);// cửa sổ hiển thị lên màn hình 640 X 480
	glutInitWindowPosition(100, 100); // vị trí xuất hiện của cửa sổ màn hình
	glutCreateWindow(argv[0]); //tạo ra một cửa sổ cấp cao nhất
	init(); // khởi tạo
	glutDisplayFunc(display); //glutDisplayFunc đặt cuộc gọi lại hiển thị cho cửa sổ hiện tại.
	glutReshapeFunc(reshape); //glutReshapeFunc thiết lập lại cuộc gọi lại cho cửa sổ hiện tại.
	glutMainLoop(); //glutMainLoop vào vòng xử lý sự kiện GLUT.
	return 0;
}
