﻿// BT_08_GL_TRIANGLE_STRIP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Dependencies\glew\glew.h"
#include "Dependencies\freeglut\freeglut.h"

const int screenWidth = 640; // khởi tạo chiều dài chiều cho màn hình
const int screenHeight = 480; // khởi tạo chiều rộng cho màn hình

void init(void)
{
	glClearColor(1.0, 1.0, 1.0, 1.0); //hàm glClearColor xóa bộ đệm màu
	glShadeModel(GL_FLAT);
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT); // Hàm glClear xóa bộ đệm thành các giá trị đặt trước.
	glColor3f(0.0, 0.0, 0.0);
	glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);

	glBegin(GL_TRIANGLES);
		glVertex2i(50, 50);//Tam giác 1
		glVertex2i(200, 50);
		glVertex2i(100, 150);

		glVertex2i(300, 100);//Tam Giác 2
		glVertex2i(450, 150);
		glVertex2i(350, 250);
	glEnd();

	glBegin(GL_TRIANGLE_STRIP);
		glVertex2i(50, 50);
		glVertex2i(100, 150);
		glVertex2i(200, 50);

		glVertex2i(350, 250);
		glVertex2i(300, 100);
		glVertex2i(450, 150);
	glEnd();
	glFlush();
}

void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h); //Hàm glViewport thiết lập chế độ xem.
	glMatrixMode(GL_PROJECTION); //Hàm glMatrixMode chỉ định ma trận nào là ma trận hiện tại(Áp dụng các hoạt động ma trận tiếp theo cho ngăn xếp ma trận chiếu.)
	glLoadIdentity(); //Hàm glLoadIdentity thay thế ma trận hiện tại bằng ma trận danh tính.
	gluOrtho2D(0.0, (GLdouble)w, 0.0, (GLdouble)h);//hàm xác định ma trận chiếu hình chính tả 2 chiều.
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv); //glutInit được sử dụng để khởi tạo thư viện GLUT.
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA); //đặt chế độ hiển thị ban đầu.
	glutInitWindowSize(screenWidth, screenHeight);// cửa sổ hiển thị lên màn hình 640 X 480
	glutInitWindowPosition(100, 100); // vị trí xuất hiện của cửa sổ màn hình
	glutCreateWindow(argv[0]); //tạo ra một cửa sổ cấp cao nhất
	init(); // khởi tạo
	glutDisplayFunc(display); //glutDisplayFunc đặt cuộc gọi lại hiển thị cho cửa sổ hiện tại.
	glutReshapeFunc(reshape); //glutReshapeFunc thiết lập lại cuộc gọi lại cho cửa sổ hiện tại.
	glutMainLoop(); //glutMainLoop vào vòng xử lý sự kiện GLUT.
	return 0;
}

