﻿// BT_12.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <math.h>
#include "Dependencies\glew\glew.h"
#include "Dependencies\freeglut\freeglut.h"
#define PI 3.14159265358979323846
#define STEPS 40
#define MAX_STARS 300

const int screenWidth = 640; // khởi tạo chiều dài chiều cho màn hình
const int screenHeight = 480; // khởi tạo chiều rộng cho màn hình

typedef struct star
{
	GLint x, y;
	GLint radius;
	GLint velocity;
	GLfloat intensity;
}STAR;

STAR  sky[MAX_STARS];

void init(void)
{
	glClearColor(0.0, 0.0, 0.0, 0.0); //hàm glClearColor xóa bộ đệm màu
}

void glCircle(GLint x, GLint y, GLint radius)
{
	GLfloat twicePi = (GLfloat)2.0f * PI;
	glBegin(GL_TRIANGLE_FAN);
	glVertex2i(x, y);
	for (int i = 0; i <= STEPS; i++) 
	{
		glVertex2i((GLint)(x + (radius *cos(i * twicePi / STEPS)) + 0.5), (GLint)(y + (radius *sin(i * twicePi / STEPS)) + 0.5));
	}
	glEnd();
}

void SkyInit()
{
	for (int i = 0; i < MAX_STARS; i++) 
	{
		sky[i].x = rand() % screenWidth;
		sky[i].y = rand() % screenHeight;
		sky[i].radius = 1 + rand() % 3;
		sky[i].intensity = sky[i].radius/3.0f;
		sky[i].velocity = sky[i].radius * 2 + rand() % 3;
	}
}

void skyDraw() 
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT);
	for (int i = 0; i < MAX_STARS; i++) {
		glColor3f(sky[i].intensity, sky[i].intensity, sky[i].intensity);
		glCircle(sky[i].x, sky[i].y, sky[i].radius);
	}
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT); // Hàm glClear xóa bộ đệm thành các giá trị đặt trước.
	glColor3f(1.0, 1.0, 1.0);

	glPushMatrix();
	skyDraw();
	glPopMatrix();
	glutSwapBuffers();
}

void update()
{
	for (int i = 0; i < MAX_STARS; i++) {
		sky[i].x += sky[i].velocity;
		if (sky[i].x >= screenWidth)
		{
			sky[i].x = rand() % screenWidth;
			sky[i].y = rand() % screenHeight;
			sky[i].radius = 1 + rand() % 3;
			sky[i].intensity = sky[i].radius / 3.0f;
			sky[i].velocity = sky[i].radius * 2 + rand() % 3;
		}
	}
	Sleep(50);
	glutPostRedisplay();
}

void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h); //Hàm glViewport thiết lập chế độ xem.
	glMatrixMode(GL_PROJECTION); //Hàm glMatrixMode chỉ định ma trận nào là ma trận hiện tại(Áp dụng các hoạt động ma trận tiếp theo cho ngăn xếp ma trận chiếu.)
	glLoadIdentity(); //Hàm glLoadIdentity thay thế ma trận hiện tại bằng ma trận danh tính.
	gluOrtho2D(0.0, (GLdouble)w, 0.0, (GLdouble)h);//hàm xác định ma trận chiếu hình chính tả 2 chiều.
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv); //glutInit được sử dụng để khởi tạo thư viện GLUT.
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA); //đặt chế độ hiển thị ban đầu.
	glutInitWindowSize(screenWidth, screenHeight);// cửa sổ hiển thị lên màn hình 640 X 480
	glutInitWindowPosition(100, 100); // vị trí xuất hiện của cửa sổ màn hình
	glutCreateWindow(argv[0]); //tạo ra một cửa sổ cấp cao nhất
	init(); // khởi tạo
	SkyInit();
	glutDisplayFunc(display); //glutDisplayFunc đặt cuộc gọi lại hiển thị cho cửa sổ hiện tại.
	glutReshapeFunc(reshape); //glutReshapeFunc thiết lập lại cuộc gọi lại cho cửa sổ hiện tại.
	glutIdleFunc(update);
	glutMainLoop(); //glutMainLoop vào vòng xử lý sự kiện GLUT.
	return 0;
}

