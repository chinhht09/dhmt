﻿// Bai2_Game_Snake.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "GAME.h" 

#ifdef _MSC_VER         /* chỉ thị liên kết với Visual C++ */ 
#   pragma comment (lib,    "sdl2main.lib") 
#   pragma comment (lib,    "sdl2.lib") 
#   pragma comment (linker, "/entry:\"mainCRTStartup\"" )  
#   pragma comment (linker, "/subsystem:WINDOWS") 
#endif 

#define main SDL_main
#define main main

int main(int argc, char ** argv)
{
	GAME g;
	gameInit(&g, SDL_FALSE);
	gameLoop(&g);
	gameShutdown(&g);
	return 0;
}


