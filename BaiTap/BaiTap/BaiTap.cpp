// BaiTap.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Dependencies\freeglut\glut.h"
#include <math.h>
#include <sys/timeb.h>
#include <sys/utime.h>
#include <stdlib.h>
#define totalStar 32
static int year = 0, day = 0;
float x = 0;
int chieu = 1;
double Tx[totalStar], Ty[totalStar], Tz[totalStar];
double Tr[totalStar];
int countT = 0;
int a[10];
double cam_X = 5, cam_Z = 5, cam_Y = 2;
double anpha = 0;
int cF = 0;
int totalTime = 0;
double kcc = 0;

int getMilliCount() {
	timeb tb;
	ftime(&tb);
	int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
	return nCount;
}
void drawT(double Tx, double Ty, double Tz, double Tr) {

	glPushMatrix();
	double red = (rand() % 11) * 1.0 / 10;
	double green = (rand() % 11) * 1.0 / 10;
	double blue = (rand() % 11) * 1.0 / 10;
	glColor3f(red, green, blue);
	glTranslatef(Tx, Ty, Tz);
	glutSolidSphere(Tr, 80, 80);
	glPopMatrix();
}
void sleep(int sleeptime)
{
	int count = 0;
	int beginsleep = getMilliCount();
	while (getMilliCount() - beginsleep < sleeptime)
	{
		count++;
	}

}
void init(void)
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_FLAT);

	for (int i = 0; i < totalStar; i++)
	{
		Tx[i] = rand() % 7 - 3;
		Ty[i] = rand() % 7 - 3;
		Tz[i] = rand() % 7 - 10;
		Tr[i] = (rand() % 5 + 1) * 1.0 / 10;
	}
	totalTime = getMilliCount();
	kcc = 5;
	cam_X = sin(anpha*3.14 / 360)*kcc;
	cam_Z = cos(anpha*3.14 / 360)*kcc;
}
void render(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glPushMatrix();
	glColor3f(1.0, 0, 0);
	glutWireSphere(1.0, 100, 16);


	//glRotatef((GLfloat)year, 0.0, 1.0, 0.0); 
	glTranslatef(x, 0.0, 0.0);
	glRotatef((GLfloat)day, 0.0, 1.0, 0.0);
	glColor3f(0, 0, 1.0);
	glutWireSphere(0.2, 10, 8);
	glPopMatrix();
	for (int i = 0; i < totalStar; i++)
	{

		drawT(Tx[i], Ty[i], Tz[i], Tr[i]);
	}

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(cam_X, cam_Y, cam_Z, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

	glutSwapBuffers();


}
int maxFrame = 50;
void update() {
	cF++;
	if (maxFrame <= 0) maxFrame = 1;
	int chuky = 1000 / maxFrame;
	int timeDiff = getMilliCount() - totalTime;
	totalTime = getMilliCount();
	if (timeDiff < chuky)
	{
		sleep(chuky - timeDiff);
	}


	if (x > 3 || x < -3) {
		chieu = chieu * -1;
	}
	x = x + 0.1 * chieu;
	for (int i = 0; i < totalStar; i++)
		if (Tz[i] > 10)
		{
			Tx[i] = rand() % 7 - 3;
			Ty[i] = rand() % 7 - 3;
			Tz[i] = rand() % 7 - 10;
			Tr[i] = (rand() % 5 + 1) * 1.0 / 10;

		}
	for (int i = 0; i<totalStar; i++)
		Tz[i] = Tz[i] + Tr[i];




	countT++;

	cam_X = kcc * sin(anpha*3.14 / 360);
	cam_Z = kcc * cos(anpha*3.14 / 360);

	glutPostRedisplay();



}

void reshape(int w, int h) {

	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, (GLfloat)w / (GLfloat)h, 1.0, 100.0);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(cam_X, cam_Y, 10.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}
void inputProcess(unsigned char key, int x, int y) {

	switch (key) {
	case 'a':
		anpha -= 1;
		break;
	case 'd':
		anpha += 1;
		break;
	case 'q':
		maxFrame--;
		break;
	case 'e':
		maxFrame++;
		break;
	case 'w':
		kcc += 0.1;
		break;
	case 's':
		kcc -= 0.1;
		break;

	default:          break;
	}
}


int main(int argc, char** argv)
{
	int chuky = 1000 / maxFrame;
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(500, 500);

	glutInitWindowPosition(100, 100);
	glutCreateWindow(argv[0]);
	init();

	glutDisplayFunc(render);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(inputProcess);
	glutIdleFunc(update);


	glutMainLoop();


	return 0;
}


